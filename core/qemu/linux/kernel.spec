Name:           kernel
Version:        4.14
Release:        1%{?dist}
Summary:        Linux kernel
BuildArch:      x86_64
Group:          Core/Kernel
License:        GPL
URL:            https://android.googlesource.com/kernel/goldfish

%description
Kernel

%package -n kernel-headers
Summary:        Development files needed for building
Group:          Development/Sources

%description -n kernel-headers
Linux headers required for development

%package -n kernel-modules
Summary:        Basic kernel modules
Group:          System/Kernel

%description -n kernel-modules
Basic kernel modules

%prep
mkdir -p linux-kernel
cd linux-kernel
if [ ! -d ".git" ] ; then
    git clone https://android.googlesource.com/kernel/goldfish -b android-goldfish-4.14-dev --depth=1 ./
fi
git fetch https://android.googlesource.com/kernel/goldfish android-goldfish-4.14-dev
git checkout FETCH_HEAD

%build
cd linux-kernel
%__cc -v
make -j8 clean
make -j8 x86_64_ranchu_defconfig
CC=%__cc make -j8

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/boot
cd linux-kernel
cp arch/x86/boot/bzImage $RPM_BUILD_ROOT/boot/vmlinuz
make INSTALL_HDR_PATH=$RPM_BUILD_ROOT/usr headers_install
make INSTALL_MOD_PATH=$RPM_BUILD_ROOT modules_install

find $RPM_BUILD_ROOT/usr -type f -name '*install.cmd' -delete
find $RPM_BUILD_ROOT/usr -type f -name '.install' -delete

%clean
rm -rf $RPM_BUILD_ROOT
cd linux-kernel
make clean

%files
%defattr(-,root,root,-)
/boot/vmlinuz


%files headers
%defattr(-,root,root,-)
/usr/include/*

%files modules
%defattr(-,root,root,-)
/lib/modules/*
