Name:           busybox
Version:        4.14
Release:        1%{?dist}
Summary:        BusyBox: The Swiss Army Knife of Embedded Linux
BuildArch:      x86_64
Group:          Core/Tools
License:        GPL
URL:            https://busybox.net/

%description
Busybox

%prep
mkdir -p busybox
cd busybox
if [ ! -d ".git" ] ; then
    git clone git://git.busybox.net/busybox -b 1_31_0 --depth=1 ./
fi
git fetch git://git.busybox.net/busybox 1_31_0
git checkout FETCH_HEAD

%build
cd busybox
%__cc -v
make -j8 clean
make -j8 android_x86_defconfig
CC=%__cc make -j8

%install

%clean

%files



