Name:           openssl
Version:        1.1.1.1
Release:        1%{?dist}
Summary:        OpenSSL

Group:          Libs/security
License:        Apache-2.0
URL:            https://github.com/openssl/openssl

%description
OpenSSL

%package -n openssl-devel
Summary:        Development files needed for building
Group:          Development/Sources

%description -n openssl-devel
OpenSSL dev files

%prep
rm -rf src
git clone https://github.com/openssl/openssl -b OpenSSL_1_1_1-stable --depth=1 src
cd src
./Configure linux-x86_64 shared no-ssl2 no-ssl3 no-comp --prefix=%{_prefix}  --libdir=%{_lib}

%build
cd src
make
%install
rm -rf $RPM_BUILD_ROOT
cd src
%make_install
#make install INSTALL_HDR_PATH=$RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
/usr/bin/*
/usr/share/*
/usr/lib64/*
/usr/ssl/*

%files devel
%defattr(-,root,root,-)
/usr/include/*
