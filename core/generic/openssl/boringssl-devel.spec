Name:           boringssl-devel
Version:        10.0.0.3
Release:        1%{?dist}
Summary:        BoringSSL
BuildArch:      noarch
Group:          Development/Sources
License:        ISC
URL:            https://android.googlesource.com/platform/external/boringssl

%description
BoringSSL include files

%prep
rm -rf src
git clone https://android.googlesource.com/platform/external/boringssl -b android-10.0.0_r3 --depth=1 src

%build

%install
rm -rf $RPM_BUILD_ROOT
cd src
mkdir -p $RPM_BUILD_ROOT/usr/include/
cp -R include/* $RPM_BUILD_ROOT/usr/include/

%files
%defattr(-,root,root,-)
/usr/include/*
