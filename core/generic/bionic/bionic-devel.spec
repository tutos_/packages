Name:           bionic-devel
Version:        10.0.0.3
Release:        1%{?dist}
Summary:        Bionic C
BuildArch:      noarch
Group:          Development/Sources
License:        BSD
URL:            https://android.googlesource.com/platform/bionic

%description
Kernel

%prep
mkdir -p bionic
cd bionic
if [ ! -d ".git" ] ; then
    git clone https://android.googlesource.com/platform/bionic -b android-10.0.0_r3 --depth=1 ./
fi
git fetch https://android.googlesource.com/platform/bionic android-10.0.0_r3
git checkout FETCH_HEAD

%install
rm -rf $RPM_BUILD_ROOT
cd bionic
mkdir -p $RPM_BUILD_ROOT/usr/include/
cp -R libc/include/* $RPM_BUILD_ROOT/usr/include/
find $RPM_BUILD_ROOT/usr/include/ | wc
cp -R libc/kernel/uapi/linux $RPM_BUILD_ROOT/usr/include/
find $RPM_BUILD_ROOT/usr/include/ | wc
cp -R libc/kernel/android/uapi/linux/* $RPM_BUILD_ROOT/usr/include/linux/
find $RPM_BUILD_ROOT/usr/include/ | wc
cp -R libc/kernel/uapi/asm-generic $RPM_BUILD_ROOT/usr/include/
find $RPM_BUILD_ROOT/usr/include/ | wc
cp -R libc/kernel/uapi/asm-x86/* $RPM_BUILD_ROOT/usr/include/
find $RPM_BUILD_ROOT/usr/include/ | wc

%files
%defattr(-,root,root,-)
/usr/include/*
